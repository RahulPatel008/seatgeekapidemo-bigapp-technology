//
//  Constants.swift
//  SeatGeekDemo
//
//  Created by Sweta on 04/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import Foundation
import SwiftDate

let cleint_ID = "MTYwNjIxNDV8MTU1NDM5MzY5MS41NQ"

func getCommonDateString(from string: String) -> String {
    guard let date = string.toISODate()?.date else {
        return ""
    }
    return date.toFormat("EEE, dd MMM yyyy HH:mm a")
}
