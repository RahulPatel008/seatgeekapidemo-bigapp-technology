//
//  APIManaer.swift
//  SeatGeekDemo
//
//  Created by Sweta on 04/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import Foundation
import Alamofire

let BASE_URL = "https://api.seatgeek.com/2/"

let eventAPI = "events"

func getEvents(of query: String = "",completion: @escaping (([Event],Bool)->(Void))) {
//    https://api.seatgeek.com/2/events?client_id=<your client id>&q=Texas+Ranger
    var str = BASE_URL+eventAPI+"?client_id=\(cleint_ID)"
    if query.count > 0 {
        str = str + "&q=\(query)"
    }
    
    var urlReq = URLRequest(url: URL(string: str)!)
    urlReq.httpMethod = "GET"
    
    Alamofire.request(urlReq).responseJSON { (response) in
        guard response.data != nil else {
            completion([Event](),false)
            return
        }
        
        guard let json = response.result.value as? [String:Any] else {
            return
        }
        let eventResponse = EventResponse(fromDictionary: json)
        completion(eventResponse.events,true)
    }.resume()
}
