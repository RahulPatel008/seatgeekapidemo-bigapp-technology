//
//  EventManager.swift
//  SeatGeekDemo
//
//  Created by Sweta on 06/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import Foundation


class EventManager {
    static let manager = EventManager()
    var events = [Event]() {
        didSet {
//            events.append(contentsOf: favEvents)
        }
    }
//    var favEvents = [Event]()
    
    private init() { }
    
    func fetchEvents(of: String = "", completion: @escaping ((Bool) -> Void)) {
        getEvents(of: of) { (allEvents, success) -> (Void) in
            self.events.append(contentsOf: allEvents)
            completion(success)
        }
    }
    
}
