//
//	EventResponse.swift
//
//	Create by Sweta on 4/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class EventResponse : NSObject, NSCoding{

	var events : [Event]!
	var inHand : Announcement!
	var meta : Meta!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		events = [Event]()
		if let eventsArray = dictionary["events"] as? [[String:Any]]{
			for dic in eventsArray{
				let value = Event(fromDictionary: dic)
				events.append(value)
			}
		}
		if let inHandData = dictionary["in_hand"] as? [String:Any]{
			inHand = Announcement(fromDictionary: inHandData)
		}
		if let metaData = dictionary["meta"] as? [String:Any]{
			meta = Meta(fromDictionary: metaData)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if events != nil{
			var dictionaryElements = [[String:Any]]()
			for eventsElement in events {
				dictionaryElements.append(eventsElement.toDictionary())
			}
			dictionary["events"] = dictionaryElements
		}
		if inHand != nil{
			dictionary["in_hand"] = inHand.toDictionary()
		}
		if meta != nil{
			dictionary["meta"] = meta.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         events = aDecoder.decodeObject(forKey :"events") as? [Event]
         inHand = aDecoder.decodeObject(forKey: "in_hand") as? Announcement
         meta = aDecoder.decodeObject(forKey: "meta") as? Meta

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if events != nil{
			aCoder.encode(events, forKey: "events")
		}
		if inHand != nil{
			aCoder.encode(inHand, forKey: "in_hand")
		}
		if meta != nil{
			aCoder.encode(meta, forKey: "meta")
		}

	}

}