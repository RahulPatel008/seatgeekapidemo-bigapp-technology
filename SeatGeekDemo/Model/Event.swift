//
//	Event.swift
//
//	Create by Sweta on 4/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Event : NSObject, NSCoding{

	var accessMethod : AnyObject!
	var announceDate : String!
	var announcements : Announcement!
	var conditional : Bool!
	var createdAt : String!
	var dateTbd : Bool!
	var datetimeLocal : String!
	var datetimeTbd : Bool!
	var datetimeUtc : String!
	var descriptionField : String!
	var eventPromotion : AnyObject!
	var generalAdmission : Bool!
	var id : Int!
	var isOpen : Bool!
	var links : [AnyObject]!
	var performers : [Performer]!
	var popularity : Int!
	var score : Int!
	var shortTitle : String!
	var stats : Stat!
	var status : String!
	var taxonomies : [Taxonomy]!
	var timeTbd : Bool!
	var title : String!
	var type : String!
	var url : String!
	var venue : Venue!
	var visibleUntilUtc : String!
    var isFavourite : Bool! = false

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		accessMethod = dictionary["access_method"] as? AnyObject
		announceDate = dictionary["announce_date"] as? String
		if let announcementsData = dictionary["announcements"] as? [String:Any]{
			announcements = Announcement(fromDictionary: announcementsData)
		}
		conditional = dictionary["conditional"] as? Bool
		createdAt = dictionary["created_at"] as? String
		dateTbd = dictionary["date_tbd"] as? Bool
		datetimeLocal = dictionary["datetime_local"] as? String
		datetimeTbd = dictionary["datetime_tbd"] as? Bool
		datetimeUtc = dictionary["datetime_utc"] as? String
		descriptionField = dictionary["description"] as? String
		eventPromotion = dictionary["event_promotion"] as? AnyObject
		generalAdmission = dictionary["general_admission"] as? Bool
		id = dictionary["id"] as? Int
		isOpen = dictionary["is_open"] as? Bool
		links = dictionary["links"] as? [AnyObject]
		performers = [Performer]()
		if let performersArray = dictionary["performers"] as? [[String:Any]]{
			for dic in performersArray{
				let value = Performer(fromDictionary: dic)
				performers.append(value)
			}
		}
		popularity = dictionary["popularity"] as? Int
		score = dictionary["score"] as? Int
		shortTitle = dictionary["short_title"] as? String
		if let statsData = dictionary["stats"] as? [String:Any]{
			stats = Stat(fromDictionary: statsData)
		}
		status = dictionary["status"] as? String
		taxonomies = [Taxonomy]()
		if let taxonomiesArray = dictionary["taxonomies"] as? [[String:Any]]{
			for dic in taxonomiesArray{
				let value = Taxonomy(fromDictionary: dic)
				taxonomies.append(value)
			}
		}
		timeTbd = dictionary["time_tbd"] as? Bool
		title = dictionary["title"] as? String
		type = dictionary["type"] as? String
		url = dictionary["url"] as? String
		if let venueData = dictionary["venue"] as? [String:Any]{
			venue = Venue(fromDictionary: venueData)
		}
		visibleUntilUtc = dictionary["visible_until_utc"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if accessMethod != nil{
			dictionary["access_method"] = accessMethod
		}
		if announceDate != nil{
			dictionary["announce_date"] = announceDate
		}
		if announcements != nil{
			dictionary["announcements"] = announcements.toDictionary()
		}
		if conditional != nil{
			dictionary["conditional"] = conditional
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if dateTbd != nil{
			dictionary["date_tbd"] = dateTbd
		}
		if datetimeLocal != nil{
			dictionary["datetime_local"] = datetimeLocal
		}
		if datetimeTbd != nil{
			dictionary["datetime_tbd"] = datetimeTbd
		}
		if datetimeUtc != nil{
			dictionary["datetime_utc"] = datetimeUtc
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if eventPromotion != nil{
			dictionary["event_promotion"] = eventPromotion
		}
		if generalAdmission != nil{
			dictionary["general_admission"] = generalAdmission
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isOpen != nil{
			dictionary["is_open"] = isOpen
		}
		if links != nil{
			dictionary["links"] = links
		}
		if performers != nil{
			var dictionaryElements = [[String:Any]]()
			for performersElement in performers {
				dictionaryElements.append(performersElement.toDictionary())
			}
			dictionary["performers"] = dictionaryElements
		}
		if popularity != nil{
			dictionary["popularity"] = popularity
		}
		if score != nil{
			dictionary["score"] = score
		}
		if shortTitle != nil{
			dictionary["short_title"] = shortTitle
		}
		if stats != nil{
			dictionary["stats"] = stats.toDictionary()
		}
		if status != nil{
			dictionary["status"] = status
		}
		if taxonomies != nil{
			var dictionaryElements = [[String:Any]]()
			for taxonomiesElement in taxonomies {
				dictionaryElements.append(taxonomiesElement.toDictionary())
			}
			dictionary["taxonomies"] = dictionaryElements
		}
		if timeTbd != nil{
			dictionary["time_tbd"] = timeTbd
		}
		if title != nil{
			dictionary["title"] = title
		}
		if type != nil{
			dictionary["type"] = type
		}
		if url != nil{
			dictionary["url"] = url
		}
		if venue != nil{
			dictionary["venue"] = venue.toDictionary()
		}
		if visibleUntilUtc != nil{
			dictionary["visible_until_utc"] = visibleUntilUtc
		}
        dictionary["isFavourite"] = isFavourite
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accessMethod = aDecoder.decodeObject(forKey: "access_method") as? AnyObject
         announceDate = aDecoder.decodeObject(forKey: "announce_date") as? String
         announcements = aDecoder.decodeObject(forKey: "announcements") as? Announcement
         conditional = aDecoder.decodeObject(forKey: "conditional") as? Bool
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         dateTbd = aDecoder.decodeObject(forKey: "date_tbd") as? Bool
         datetimeLocal = aDecoder.decodeObject(forKey: "datetime_local") as? String
         datetimeTbd = aDecoder.decodeObject(forKey: "datetime_tbd") as? Bool
         datetimeUtc = aDecoder.decodeObject(forKey: "datetime_utc") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         eventPromotion = aDecoder.decodeObject(forKey: "event_promotion") as? AnyObject
         generalAdmission = aDecoder.decodeObject(forKey: "general_admission") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isOpen = aDecoder.decodeObject(forKey: "is_open") as? Bool
         links = aDecoder.decodeObject(forKey: "links") as? [AnyObject]
         performers = aDecoder.decodeObject(forKey :"performers") as? [Performer]
         popularity = aDecoder.decodeObject(forKey: "popularity") as? Int
         score = aDecoder.decodeObject(forKey: "score") as? Int
         shortTitle = aDecoder.decodeObject(forKey: "short_title") as? String
         stats = aDecoder.decodeObject(forKey: "stats") as? Stat
         status = aDecoder.decodeObject(forKey: "status") as? String
         taxonomies = aDecoder.decodeObject(forKey :"taxonomies") as? [Taxonomy]
         timeTbd = aDecoder.decodeObject(forKey: "time_tbd") as? Bool
         title = aDecoder.decodeObject(forKey: "title") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
         venue = aDecoder.decodeObject(forKey: "venue") as? Venue
         visibleUntilUtc = aDecoder.decodeObject(forKey: "visible_until_utc") as? String
         isFavourite = aDecoder.decodeObject(forKey: "isFavourite") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accessMethod != nil{
			aCoder.encode(accessMethod, forKey: "access_method")
		}
		if announceDate != nil{
			aCoder.encode(announceDate, forKey: "announce_date")
		}
		if announcements != nil{
			aCoder.encode(announcements, forKey: "announcements")
		}
		if conditional != nil{
			aCoder.encode(conditional, forKey: "conditional")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if dateTbd != nil{
			aCoder.encode(dateTbd, forKey: "date_tbd")
		}
		if datetimeLocal != nil{
			aCoder.encode(datetimeLocal, forKey: "datetime_local")
		}
		if datetimeTbd != nil{
			aCoder.encode(datetimeTbd, forKey: "datetime_tbd")
		}
		if datetimeUtc != nil{
			aCoder.encode(datetimeUtc, forKey: "datetime_utc")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if eventPromotion != nil{
			aCoder.encode(eventPromotion, forKey: "event_promotion")
		}
		if generalAdmission != nil{
			aCoder.encode(generalAdmission, forKey: "general_admission")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isOpen != nil{
			aCoder.encode(isOpen, forKey: "is_open")
		}
		if links != nil{
			aCoder.encode(links, forKey: "links")
		}
		if performers != nil{
			aCoder.encode(performers, forKey: "performers")
		}
		if popularity != nil{
			aCoder.encode(popularity, forKey: "popularity")
		}
		if score != nil{
			aCoder.encode(score, forKey: "score")
		}
		if shortTitle != nil{
			aCoder.encode(shortTitle, forKey: "short_title")
		}
		if stats != nil{
			aCoder.encode(stats, forKey: "stats")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if taxonomies != nil{
			aCoder.encode(taxonomies, forKey: "taxonomies")
		}
		if timeTbd != nil{
			aCoder.encode(timeTbd, forKey: "time_tbd")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
		if venue != nil{
			aCoder.encode(venue, forKey: "venue")
		}
		if visibleUntilUtc != nil{
			aCoder.encode(visibleUntilUtc, forKey: "visible_until_utc")
		}
        aCoder.encode(isFavourite, forKey: "isFavourite")
	}

}
