//
//	Taxonomy.swift
//
//	Create by Sweta on 4/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Taxonomy : NSObject, NSCoding{

	var documentSource : DocumentSource!
	var id : Int!
	var name : String!
	var parentId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		if let documentSourceData = dictionary["document_source"] as? [String:Any]{
			documentSource = DocumentSource(fromDictionary: documentSourceData)
		}
		id = dictionary["id"] as? Int
		name = dictionary["name"] as? String
		parentId = dictionary["parent_id"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if documentSource != nil{
			dictionary["document_source"] = documentSource.toDictionary()
		}
		if id != nil{
			dictionary["id"] = id
		}
		if name != nil{
			dictionary["name"] = name
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         documentSource = aDecoder.decodeObject(forKey: "document_source") as? DocumentSource
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if documentSource != nil{
			aCoder.encode(documentSource, forKey: "document_source")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}

	}

}
