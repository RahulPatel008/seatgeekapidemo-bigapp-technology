//
//	Stat.swift
//
//	Create by Sweta on 4/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Stat : NSObject, NSCoding{

	var eventCount : Int!
	var averagePrice : Int!
	var dqBucketCounts : [Int]!
	var highestPrice : Int!
	var listingCount : Int!
	var lowestPrice : Int!
	var lowestPriceGoodDeals : AnyObject!
	var medianPrice : Int!
	var visibleListingCount : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		eventCount = dictionary["event_count"] as? Int
		averagePrice = dictionary["average_price"] as? Int
		dqBucketCounts = dictionary["dq_bucket_counts"] as? [Int]
		highestPrice = dictionary["highest_price"] as? Int
		listingCount = dictionary["listing_count"] as? Int
		lowestPrice = dictionary["lowest_price"] as? Int
		lowestPriceGoodDeals = dictionary["lowest_price_good_deals"] as? AnyObject
		medianPrice = dictionary["median_price"] as? Int
		visibleListingCount = dictionary["visible_listing_count"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if eventCount != nil{
			dictionary["event_count"] = eventCount
		}
		if averagePrice != nil{
			dictionary["average_price"] = averagePrice
		}
		if dqBucketCounts != nil{
			dictionary["dq_bucket_counts"] = dqBucketCounts
		}
		if highestPrice != nil{
			dictionary["highest_price"] = highestPrice
		}
		if listingCount != nil{
			dictionary["listing_count"] = listingCount
		}
		if lowestPrice != nil{
			dictionary["lowest_price"] = lowestPrice
		}
		if lowestPriceGoodDeals != nil{
			dictionary["lowest_price_good_deals"] = lowestPriceGoodDeals
		}
		if medianPrice != nil{
			dictionary["median_price"] = medianPrice
		}
		if visibleListingCount != nil{
			dictionary["visible_listing_count"] = visibleListingCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         eventCount = aDecoder.decodeObject(forKey: "event_count") as? Int
         averagePrice = aDecoder.decodeObject(forKey: "average_price") as? Int
         dqBucketCounts = aDecoder.decodeObject(forKey: "dq_bucket_counts") as? [Int]
         highestPrice = aDecoder.decodeObject(forKey: "highest_price") as? Int
         listingCount = aDecoder.decodeObject(forKey: "listing_count") as? Int
         lowestPrice = aDecoder.decodeObject(forKey: "lowest_price") as? Int
         lowestPriceGoodDeals = aDecoder.decodeObject(forKey: "lowest_price_good_deals") as? AnyObject
         medianPrice = aDecoder.decodeObject(forKey: "median_price") as? Int
         visibleListingCount = aDecoder.decodeObject(forKey: "visible_listing_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if eventCount != nil{
			aCoder.encode(eventCount, forKey: "event_count")
		}
		if averagePrice != nil{
			aCoder.encode(averagePrice, forKey: "average_price")
		}
		if dqBucketCounts != nil{
			aCoder.encode(dqBucketCounts, forKey: "dq_bucket_counts")
		}
		if highestPrice != nil{
			aCoder.encode(highestPrice, forKey: "highest_price")
		}
		if listingCount != nil{
			aCoder.encode(listingCount, forKey: "listing_count")
		}
		if lowestPrice != nil{
			aCoder.encode(lowestPrice, forKey: "lowest_price")
		}
		if lowestPriceGoodDeals != nil{
			aCoder.encode(lowestPriceGoodDeals, forKey: "lowest_price_good_deals")
		}
		if medianPrice != nil{
			aCoder.encode(medianPrice, forKey: "median_price")
		}
		if visibleListingCount != nil{
			aCoder.encode(visibleListingCount, forKey: "visible_listing_count")
		}

	}

}