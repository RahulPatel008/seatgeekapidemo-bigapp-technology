//
//  EventListController.swift
//  SeatGeekDemo
//
//  Created by Sweta on 04/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import UIKit
import Motion

class EventListController: UITableViewController {
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchController()
        if EventManager.manager.events.count == 0 {
            EventManager.manager.fetchEvents { (success) in
                self.tableView.reloadData()
            }
        }
        
        isMotionEnabled = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 42/255.0, green: 70/255.0, blue: 88/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        tableView.reloadData()
    }
    
    func setupSearchController() {
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.barStyle = .black
        searchController.searchBar.delegate = self
        searchController.searchBar.clipsToBounds = true //For close button, while text is in searchbar field
        searchController.searchBar.placeholder = "Search Events"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EventManager.manager.events.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventCell
        let event = EventManager.manager.events[indexPath.row]
        cell.event = event
        cell.imgView.motionIdentifier = event.id.description
        cell.favImgView.isHidden = !event.isFavourite
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailController") as! EventDetailController
        vc.event = EventManager.manager.events[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension EventListController: UISearchBarDelegate {
//    // MARK: - UISearchResultsUpdating Delegate
//    func updateSearchResults(for searchController: UISearchController) {
//        // TODO
////        filterContentForSearchText(searchController.searchBar.text!)
//        getEvents(of: searchController.searchBar.text!) { (arrEvent, success) -> (Void) in
//            self.events = arrEvent
//            self.tableView.reloadData()
//        }
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.dismiss(animated: true, completion: nil)
        EventManager.manager.fetchEvents(of: searchBar.text!) { (success) in
            self.tableView.reloadData()
        }
    }
    
}
