//
//  EventNavigationController.swift
//  SeatGeekDemo
//
//  Created by Sweta on 06/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import UIKit
import Motion

class EventNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        isMotionEnabled = true
        motionTransitionType = .zoom
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
