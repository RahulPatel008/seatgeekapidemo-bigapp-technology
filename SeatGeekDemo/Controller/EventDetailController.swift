//
//  EventDetailController.swift
//  SeatGeekDemo
//
//  Created by Sweta on 06/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import UIKit
import Motion

class EventDetailController: UIViewController {
    
    var event: Event!
    @IBOutlet weak var eventImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isMotionEnabled = true
        eventImgView.motionIdentifier = event.id.description
        motionTransitionType = .autoReverse(presenting: .zoom)
        titleLbl.text = event.title
        timeLbl.text = getCommonDateString(from: event.announceDate)
        locationLbl.text = event.venue.displayLocation
        
        guard event.performers.count > 0, let imageLink = event.performers[0].image else {
            return
        }
        eventImgView.sd_setImage(with: URL(string: imageLink)!, completed: nil)
        favouriteBtn.isSelected = event.isFavourite
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func backClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func favouriteClicked(_ sender: UIButton) {
        event.isFavourite = !event.isFavourite
        sender.isSelected = event.isFavourite
    }
}
