//
//  EventCell.swift
//  SeatGeekDemo
//
//  Created by Sweta on 04/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import UIKit
import SDWebImage

class EventCell: UITableViewCell {
    
    var event: Event! {
        didSet {
            titleLbl.text = event.title
            venueLbl.text = event.venue.displayLocation
            timeLbl.text = getCommonDateString(from: event.announceDate)
            
            favImgView.isHidden = true
            //assign Image
            if event.performers.count > 0 {
                if let str = event.performers[0].image {
                    imgView?.sd_setImage(with: URL(string: str), completed: { (image, error, cacheType, url) in
                        if image == nil {
                            self.imgView.image = nil
                        }
                    })
                } else {
                    imgView.image = nil
                }
            } else {
                imgView.image = nil
            }
        }
    }
    
    @IBOutlet weak var favImgView: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var venueLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
